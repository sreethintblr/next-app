import Head from 'next/head'
import styles from '../styles/Home.module.css'
import About from './about'
import Footer from './footer'
import Header from './header'
export default function Home() {
  return (
    <div> <Header></Header>
      <div className={styles.container}>
        <Head>
          <title>My Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <About></About>

        <footer className={styles.footer}>
          <p></p>
          <Footer></Footer>
        </footer>
      </div>
    </div>
  )
}
