import styles from '../styles/Home.module.css'

function Header() {
    return <div className={styles.header}>
        <a href="" className={styles.logo}>Toobler</a>
        <div className={styles.header_right}>
            <a className={styles.active} href="">Home</a>
            <a href="/contactus">Contact</a>
            <a href="/about">About</a>
        </div>
    </div>
}

export default Header