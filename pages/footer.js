import styles from '../styles/Home.module.css'

function Footer() {
    return <footer><div class="fixed-footer">
        <div class="container">Copyright &copy; 2020 Toobler</div>
    </div></footer>
}

export default Footer